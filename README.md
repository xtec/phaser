# Phaser


Pages are served under: https://xtec.gitlab.io/phaser/


## Develop

With Firefox set:

about:config
security.fileuri.strict_origin_policy

## Pages

https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_ui.html

https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_forked_sample_project.html

## Learn

http://phaser.io/tutorials/making-your-first-phaser-3-game-spanish